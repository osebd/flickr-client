import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers";
import createSagaMiddleware from 'redux-saga';
import rootSaga from "../sagas";


const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleWare = createSagaMiddleware()
const store = createStore(rootReducer, composeEnhancer(applyMiddleware(sagaMiddleWare)));
sagaMiddleWare.run(rootSaga)

export default store;