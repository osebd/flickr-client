import { put, takeEvery, all, call } from 'redux-saga/effects';
import { CONSTANTS, storeData, composeData } from './../actions'
import axios from 'axios'

function* fetchFirstPage(action) {
    let data 
    yield axios.get(process.env.REACT_APP_PROXY_URL + action.payload.url, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        params: {
            'per_page': 20,
            'page': 1,
            'text': action.payload.text,
            'safe_search': 3,
            'content_type': 7
        }
      })
    .then(res => data = res.data.photos)
              
    yield put(storeData(data.photo))
}

function* fetchNextPage(action) {
    let data

    yield axios.get(process.env.REACT_APP_PROXY_URL + action.payload.url, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        params: {
            'per_page': 20,
            'page': action.payload.pageNumber,
            'text': action.payload.text,
            'safe_search': 3,
            'content_type': 7
        }
      })
    .then(res => data = res.data.photos)
      
    yield put(composeData(data.photo))
}



function* watchInitSaga() {
    yield takeEvery(CONSTANTS.INIT_DATA, fetchFirstPage)
}

function* watchScrollSaga() {
  yield takeEvery(CONSTANTS.FETCH_NEXT_PAGE, fetchNextPage)
}

export default function* rootSaga() {
    yield all([
        watchInitSaga(),
        watchScrollSaga()
    ])
}