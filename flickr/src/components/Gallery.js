import React from 'react';

const Image = ({ image }) => {
    return (
        <img className='picture' src={`https://farm${image.farm}.staticflickr.com/${image.server}/${
            image.id
            }_${image.secret}.jpg`} />
    )
}

export default class Gallery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (<div className='Gallery'>
            {this.props.photos.map((image, key) => {
                return (
                    <Image image={image} key={key} />
                )
            })}
        </div>)
    }
}