import React from 'react';
import { SearchOutlined } from '@ant-design/icons'
import store from './../store/index'
import { connect } from 'react-redux'
import _ from 'lodash'
import { fetchNextPage, fetchFirstPage } from '../actions';

class SearchBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            searchValue: null
        }

    }

    performQuery = _.debounce((text) => {
        store.dispatch(fetchFirstPage(process.env.REACT_APP_QUERY_URL, text))
        this.props.callBack(text)
    }, 400)

    handleChange = (event) => {
        const value = event.target.value
        this.setState({ searchValue: value })
        this.performQuery(value)
    }

    render() {
        return (<div className='SearchBar'>
            <SearchOutlined className='img' />
            <input
                className='input'
                placeholder='Search for photo'
                value={this.state.searchValue || ''}
                onChange={this.handleChange}
                autoFocus
            />
        </div>
        )
    }
}

const mapStateToProps = function (state) {
    return {
      photos: state.photos
    }
  }
export default connect(mapStateToProps)(SearchBar);