import React from 'react';
import store from './../store/index'
import { fetchFirstPage, fetchNextPage } from './../actions'
import { connect } from 'react-redux'
import Gallery from './Gallery';
import SearchBar from './SearchBar';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentPage: 1,
      text: '',
    }
  }

  setInputText = (value) => {
    this.setState({ text: value })
  }

  turnPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 },
      () => store.dispatch(fetchNextPage(process.env.REACT_APP_QUERY_URL, this.state.text, this.state.currentPage)))
  }

  componentDidMount() {
    window.onscroll = () => {
      const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight) {
            this.turnPage()
          }
    };

    store.dispatch(fetchFirstPage(process.env.REACT_APP_QUERY_URL))
  }

  render() {
    return (
      <div className="App">
        <div className='header'>Flickr Assignment</div>
        <div className='content'>
          <SearchBar callBack={this.setInputText} />
          <Gallery photos={this.props.photos} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    photos: state.photos
  }
}

export default connect(mapStateToProps)(App);
