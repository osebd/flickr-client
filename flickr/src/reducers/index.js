import { combineReducers } from "redux";
import { CONSTANTS } from '../actions';

const initialState = []

const flickrReducer = (state = initialState, action) => {
    let newState

    switch (action.type) {
        case CONSTANTS.STORE_DATA:
            newState = [...initialState, ...action.payload.data]
            return newState;
        case CONSTANTS.COMPOSE_DATA:
            newState = [...state, ...action.payload.data]
            return newState;
        default:
            return state;
    }
}

export default combineReducers({
    photos: flickrReducer
});