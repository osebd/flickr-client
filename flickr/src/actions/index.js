export const CONSTANTS = {
    INIT_DATA: 'INIT_DATA',
    STORE_DATA: 'STORE_DATA',
    FETCH_NEXT_PAGE: 'FETCH_NEXT_PAGE',
    COMPOSE_DATA: 'COMPOSE_DATA'
}

export function fetchFirstPage(url, text) {
    return {
        type: CONSTANTS.INIT_DATA,
        payload: { url, text }
    }
}

export function fetchNextPage(url, text, pageNumber) {
    return {
        type: CONSTANTS.FETCH_NEXT_PAGE,
        payload: { url, text, pageNumber }
    }
}

export function storeData(data) {
    return {
        type: CONSTANTS.STORE_DATA,
        payload: { data } 
    }
}

export function composeData(data) {
    return {
        type: CONSTANTS.COMPOSE_DATA,
        payload: { data } 
    }
}